export const details = {
  thumb_url: 'https://s3.amazonaws.com/bit-photos/thumb/6586103.jpeg',
  facebook_page_url: 'https://www.facebook.com/insomniumofficial',
  name: 'Insomnium',
};

export const events = [
  {
    id: '1002215699',
    datetime: '2018-06-13T19:00:00',
    venue: {
      country: 'United States',
      city: 'Jefferson',
      name: 'Southport Hall',
    },
  },
];
