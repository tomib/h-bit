# Bands in Town

Demo: http://www.tomislavbisof.com/h-bit

Project template based on react-init: https://github.com/tomislavbisof/react-init

Basic caching is implemented - searching for an artist or band that exists saves their data to localStorage, while clearing the input and searching for an empty string clears it.

## Includes

* [Styled Components](https://github.com/styled-components/styled-components)
* [ESLint](https://eslint.org)
* [Airbnb Style Guide](https://github.com/airbnb/javascript)
* [Webpack 4](https://webpack.js.org)
* [React Router](https://reacttraining.com/react-router)
* [Axios](https://github.com/axios/axios)
* [PropTypes](https://github.com/facebook/prop-types)
* [Enzyme](http://airbnb.io/enzyme/)
* [Jest](https://facebook.github.io/jest/)

### Installation

To run the project, first install all the dependencies using:

```
npm install
```

### Usage

For development with the Webpack Dev Server and HMR, use:

```
npm run watch
```

To build the project for production, use:

```
npm run build
```

Some basic unit tests for all components are also available:

```
npm run test
```

Your project files will be located in the `/dist/` folder after building. Images smaller than 8KB will be embedded, while larger ones will be located in `/dist/assets/`.