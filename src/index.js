import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { injectGlobal } from 'styled-components';

import App from './components/app/App';

import { colors } from './constants/colors';

ReactDOM.render(
  <Router history={createBrowserHistory()}>
    <App />
  </Router>,
  document.getElementById('root'),
);

injectGlobal`
  html, body, h1, p, ul {
    margin: 0;
    padding: 0;
  }

  * {
    box-sizing: border-box;
  }

  body {
    background: ${colors.body};
    color: ${colors.text};

    font-family: 'Montserrat', sans-serif;
    font-size: 14px;
    line-height: 1;
  }

  a {
    color: inherit;
    text-decoration: none;
  }

  button, input, select {
    background: transparent;

    font-family: 'Montserrat', sans-serif;

    border: 0;
    outline: none;
  }
`;
