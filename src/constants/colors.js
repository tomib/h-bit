export const colors = {
  primary: '#f5674c',
  secondary: '#e2e4e4',
  base: '#ffffff',
  body: '#f9f9f9',
  text: '#2C3638',
  borders: '#e2e4e4',
};

export default colors;
