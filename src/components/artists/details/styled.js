import styled from 'styled-components';

import { colors } from '../../../constants/colors';

export const DetailsStyled = styled.div`
  padding: 40px 20px;

  @media (min-width: 760px) {
    padding: 40px;
  }
`;

export const Title = styled.h1`
  margin-bottom: 20px;

  font-size: 16px;
  font-weight: 700;
  text-align: center;

  @media (min-width: 480px) {
    margin-bottom: 40px;

    font-size: 18px;
    text-align: left;
  }
`;

export const Information = styled.div`
  display: block;

  width: 100%;

  margin-bottom: 20px;
  padding-bottom: 20px;

  @media (min-width: 480px) {
    display: table;
  }
`;

export const Left = styled.div`
  display: block;

  width: 125px;
  min-width: 125px;
  height: 125px;

  margin: 0 auto 20px auto;

  @media (min-width: 480px) {
    display: table-cell;
    vertical-align: middle;

    margin-bottom: 0;
  }

  @media (min-width: 960px) {
    width: 150px;
    min-width: 150px;
    height: 150px;
  }
`;

export const Right = styled.div`
  display: block;

  width: 100%;

  text-align: center;

  @media (min-width: 480px) {
    display: table-cell;
    vertical-align: middle;

    padding-left: 40px;

    text-align: left;
  }
`;

export const Image = styled.img`
  display: block;
  
  width: 125px;
  height: 125px;

  border: 1px solid ${colors.borders};

  @media (min-width: 960px) {
    width: 150px;
    height: 150px;
  }
`;

export const Name = styled.div`
  margin-bottom: 15px;

  font-size: 22px;
  font-weight: 700;

  @media (min-width: 480px) {
    margin-bottom: 30px;
  }

  @media (min-width: 960px) {
    font-size: 28px;
  }
`;

export const Facebook = styled.a`
  display: block;

  color: ${colors.primary};

  font-size: 11px;
  word-break: break-all;

  @media (min-width: 480px) {
    font-size: 13px;
  }

  @media (min-width: 960px) {
    font-size: 14px;
  }
`;

export const Empty = styled.div`
  font-size: 12px;
`;

