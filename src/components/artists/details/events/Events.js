import React from 'react';
import PropTypes from 'prop-types';

import Event from './event/Event';

import { EventsStyled, Header, Cell } from './styled';

const Events = props => (
  <EventsStyled data-testid="events">
    <Header>
      <Cell>Date</Cell>
      <Cell>Venue</Cell>
      <Cell>City</Cell>
      <Cell>Country</Cell>
    </Header>
    {props.events.map(item => <Event key={item.id} event={item} />)}
  </EventsStyled>
);

Events.propTypes = {
  events: PropTypes.array.isRequired,
};

export default Events;
