import React from 'react';
import { shallow } from 'enzyme';
import Events from './Events';

import { events } from '../../../../../config/dummyData';

describe('tests the Events component', () => {
  const wrapper = shallow(<Events events={events} />);

  it('renders the Events component', () => {
    expect(wrapper.find('[data-testid="events"]').exists()).toBe(true);
  });
});
