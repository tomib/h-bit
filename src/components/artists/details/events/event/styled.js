import styled from 'styled-components';

import { colors } from '../../../../../constants/colors';

export const EventStyled = styled.div`
  display: block;

  width: 100%;

  padding: 15px 20px; 

  transition: 0.2s all ease-in-out;

  &:hover {
    background: ${colors.primary};
    color: ${colors.base};
  }

  &:not(:last-child) {
    border-bottom: 1px solid ${colors.borders};
  }

  @media (min-width: 480px) {
    display: table-row;
  }
`;

export const Cell = styled.div`
  display: block;

  margin-bottom: 5px;

  line-height: 1.3;

  @media (min-width: 480px) {
    display: table-cell;
    vertical-align: middle;

    padding: 10px 0;
  }

  @media (min-width: 960px) {
    padding: 15px 0;
  }
`;

export const Label = styled.div`
  display: inline-block;

  min-width: 55px;

  margin-right: 5px;

  font-weight: 700;

  @media (min-width: 480px) {
    display: none;
  }
`;
