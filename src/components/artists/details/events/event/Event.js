import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

import { EventStyled, Cell, Label } from './styled';

const Event = props => (
  <EventStyled data-testid="event">
    <Cell data-testid="eventDate">
      <Label>Date:</Label>
      {moment(props.event.datetime).format('MMMM Do YYYY')}
    </Cell>
    <Cell data-testid="eventVenue">
      <Label>Venue:</Label>
      {props.event.venue.name}
    </Cell>
    <Cell data-testid="eventCity">
      <Label>City:</Label>
      {props.event.venue.city}
    </Cell>
    <Cell data-testid="eventCountry">
      <Label>Country:</Label>
      {props.event.venue.country}
    </Cell>
  </EventStyled>
);

Event.propTypes = {
  event: PropTypes.shape({
    datetime: PropTypes.string,
    venue: PropTypes.shape({
      city: PropTypes.string,
      country: PropTypes.string,
      name: PropTypes.string,
    }),
  }).isRequired,
};

export default Event;
