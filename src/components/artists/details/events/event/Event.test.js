import React from 'react';
import { shallow } from 'enzyme';
import Event from './Event';

import { events } from '../../../../../../config/dummyData';

describe('tests the Event component', () => {
  const wrapper = shallow(<Event event={events[0]} />);

  it('renders the Event component', () => {
    expect(wrapper.find('[data-testid="event"]').exists()).toBe(true);
  });

  it('renders the Event Date component', () => {
    expect(wrapper.find('[data-testid="eventDate"]').exists()).toBe(true);
  });

  it('renders the Event Venue component', () => {
    expect(wrapper.find('[data-testid="eventVenue"]').exists()).toBe(true);
  });

  it('renders the Event City component', () => {
    expect(wrapper.find('[data-testid="eventCity"]').exists()).toBe(true);
  });

  it('renders the Event Country component', () => {
    expect(wrapper.find('[data-testid="eventCountry"]').exists()).toBe(true);
  });
});
