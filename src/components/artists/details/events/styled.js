import styled from 'styled-components';

import { colors } from '../../../../constants/colors';

export const EventsStyled = styled.div`
  display: table;
  table-layout: fixed;
  border-collapse: collapse;
  
  width: 100%;

  font-size: 10px;
  text-align: left;

  border: 1px solid ${colors.borders};

  @media (min-width: 480px) {
    text-align: center;
  }

  @media (min-width: 760px) {
    font-size: 11px;
  }

  @media (min-width: 960px) {
    font-size: 12px;
  }
`;

export const Header = styled.div`
  display: none;

  @media (min-width: 480px) {
    display: table-row;

    border-bottom: 1px solid ${colors.borders};
  }
`;

export const Cell = styled.div`
  display: table-cell;
  vertical-align: middle;

  padding: 10px 0;
  
  font-weight: 700;

  @media (min-width: 960px) {
    padding: 15px 0;
  }
`;
