import React from 'react';
import PropTypes from 'prop-types';

import Events from './events/Events';

import {
  DetailsStyled,
  Title,
  Information,
  Left,
  Right,
  Image,
  Name,
  Facebook,
  Empty,
} from './styled';

const Details = props => (
  <DetailsStyled data-testid="details">
    <Title>Artist Information:</Title>
    <Information>
      <Left>
        <Image data-testid="detailsImage" src={props.details.thumb_url} alt="Artist Image" />
      </Left>
      <Right>
        <Name data-testid="detailsName">{props.details.name}</Name>
        <Facebook data-testid="detailsFacebook" href={props.details.facebook_page_url} target="_blank">{props.details.facebook_page_url}</Facebook>
      </Right>
    </Information>
    <Title>Upcoming events:</Title>
    {props.events.length > 0 ? (
      <Events data-testid="detailsEvents" events={props.events} />
    ) : (
      <Empty>No upcoming events have been found at this time.</Empty>
    )}
  </DetailsStyled>
);

Details.propTypes = {
  details: PropTypes.shape({
    facebook_page_url: PropTypes.string,
    name: PropTypes.string,
    thumb_url: PropTypes.string,
  }).isRequired,
  events: PropTypes.array.isRequired,
};

export default Details;
