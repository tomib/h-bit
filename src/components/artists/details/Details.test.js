import React from 'react';
import { shallow } from 'enzyme';
import Details from './Details';

import { details, events } from '../../../../config/dummyData';

describe('tests the Details component', () => {
  const wrapper = shallow(<Details details={details} events={events} />);

  it('renders the Details component', () => {
    expect(wrapper.find('[data-testid="details"]').exists()).toBe(true);
  });

  it('renders the Details Image component', () => {
    expect(wrapper.find('[data-testid="detailsImage"]').exists()).toBe(true);
  });

  it('renders the Details Name component', () => {
    expect(wrapper.find('[data-testid="detailsName"]').exists()).toBe(true);
  });

  it('renders the Details Facebook component', () => {
    expect(wrapper.find('[data-testid="detailsFacebook"]').exists()).toBe(true);
  });

  it('renders the Details Events component', () => {
    expect(wrapper.find('[data-testid="detailsEvents"]').exists()).toBe(true);
  });
});
