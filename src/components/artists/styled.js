import styled from 'styled-components';

export const ArtistsStyled = styled.div`
  overflow: hidden;
`;

export const Message = styled.div`
  display: block;
  position: relative;

  width: 100%;
  
  padding: 200px 0;
`;

export const Image = styled.img`
  display: block;

  height: 60px;

  margin: 0 auto 20px auto;
`;

export const Text = styled.div`
  text-align: center;
`;
