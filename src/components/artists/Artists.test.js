import React from 'react';
import { shallow } from 'enzyme';
import Artists from './Artists';

describe('tests the Artists component', () => {
  const wrapper = shallow(<Artists />);

  it('renders the Artists component', () => {
    expect(wrapper.find('[data-testid="artists"]').exists()).toBe(true);
  });
});
