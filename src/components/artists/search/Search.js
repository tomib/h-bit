import React from 'react';
import PropTypes from 'prop-types';

import { SearchStyled, Left, Right, Input, Button } from './styled';

const Search = props => (
  <SearchStyled data-testid="search" onSubmit={e => props.onSubmit(e)}>
    <Left>
      <Input
        data-testid="searchInput"
        name="term"
        onChange={e => props.onInputChange(e.target.value)}
        placeholder="Please enter an artist or band name here..."
        type="text"
        value={props.term}
      />
    </Left>
    <Right>
      <Button data-testid="searchButton" type="submit">Search</Button>
    </Right>
  </SearchStyled>
);

Search.propTypes = {
  onInputChange: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  term: PropTypes.string.isRequired,
};

export default Search;
