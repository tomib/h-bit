import React from 'react';
import { shallow } from 'enzyme';
import Search from './Search';

describe('tests the Search component', () => {
  const mockOnInputChange = jest.fn();

  const wrapper = shallow(<Search
    onInputChange={mockOnInputChange}
    onSubmit={() => {}}
    term=""
  />);

  it('renders the Search component', () => {
    expect(wrapper.find('[data-testid="search"]').exists()).toBe(true);
  });

  it('renders the Search Input component', () => {
    expect(wrapper.find('[data-testid="searchInput"]').exists()).toBe(true);
  });

  it('triggers the onChange event of the Search Input component', () => {
    wrapper.find('[data-testid="searchInput"]').simulate('change', { target: { value: 'T' } });
    expect(mockOnInputChange).toBeCalled();
  });

  it('renders the Search Button component', () => {
    expect(wrapper.find('[data-testid="searchButton"]').exists()).toBe(true);
  });
});
