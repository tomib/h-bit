import styled from 'styled-components';

import { colors } from '../../../constants/colors';

export const SearchStyled = styled.form`
  display: block;

  width: 100%;

  padding: 10px;

  box-shadow: 0px 2px 15px 0px rgba(0,0,0,0.10);

  @media (min-width: 480px) {
    display: table;
  }

  @media (min-width: 760px) {
    padding: 20px;
  }
`;

export const Left = styled.div`
  display: block;

  width: 100%;

  @media (min-width: 480px) {
    display: table-cell;
    vertical-align: middle;
  }
`;

export const Right = styled.div`
  display: block;

  width: 100%;

  @media (min-width: 480px) {
    display: table-cell;
    vertical-align: middle;

    width: 120px;
  }
`;

export const Input = styled.input`
  display: block;

  width: 100%;

  padding: 10px 15px;

  font-size: 11px;

  border: 1px solid ${colors.borders};

  @media (min-width: 480px) {
    font-size: 13px;

    border: 0;
  }
`;

export const Button = styled.button`
  display: block;

  width: 100%;

  padding: 10px 15px;

  background: ${colors.primary};
  color: ${colors.base};

  font-size: 11px;
  font-weight: 700;

  cursor: pointer;

  @media (min-width: 480px) {
    width: 120px;

    font-size: 13px;
  }
`;
