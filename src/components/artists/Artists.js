import React, { Component } from 'react';
import axios from 'axios';

import Search from './search/Search';
import Details from './details/Details';

import { ArtistsStyled, Message, Image, Text } from './styled';

import IconLoading from '../../assets/loading.svg';
import IconEmpty from '../../assets/band.svg';

class Artists extends Component {
  state = {
    details: {},
    events: [],
    term: '',
    loading: false,
  }

  /* Tries to load cached data on application load. */
  componentDidMount() {
    this.loadData();
  }

  /* Handles search input change. */
  onInputChange = (term) => {
    this.setState(() => ({
      term,
    }));
  }

  /* Handles search form submission. */
  onSubmit = (e) => {
    e.preventDefault();

    this.getDetails();
  }

  /* Gets details of the search term from the API and triggers event fetching on success. If the search term is empty, it clears state and cached data. */
  getDetails = () => {
    if (this.state.term) {
      this.setState({
        loading: true,
      });

      axios.get(`https://rest.bandsintown.com/artists/${this.state.term}`, {
        params: {
          app_id: '123',
        },
      }).then((response) => {
        this.setState({
          details: response.data,
        });

        this.getEvents();
      }).catch(() => {
        this.setState({
          loading: false,
        });
      });
    } else {
      this.setState({
        details: {},
        events: [],
      });

      this.clearData();
    }
  }

  /* Gets events for the artist or band and triggers data caching on success. */
  getEvents = () => {
    axios.get(`https://rest.bandsintown.com/artists/${this.state.term}/events`, {
      params: {
        app_id: '123',
      },
    }).then((response) => {
      this.setState({
        events: response.data,
        loading: false,
      });

      this.saveData();
    }).catch(() => {
      this.setState({
        loading: false,
      });
    });
  }

  /* Loads data from localStorage. */
  loadData = () => {
    this.setState({
      details: JSON.parse(localStorage.getItem('details')) || {},
      events: JSON.parse(localStorage.getItem('events')) || [],
    });
  }

  /* Saves data to localStorage. */
  saveData = () => {
    localStorage.setItem('details', JSON.stringify(this.state.details));
    localStorage.setItem('events', JSON.stringify(this.state.events));
  }

  /* Clears data from localStorage. */
  clearData = () => {
    localStorage.clear();
  }

  render() {
    return (
      <ArtistsStyled data-testid="artists">
        <Search term={this.state.term} onInputChange={this.onInputChange} onSubmit={this.onSubmit} />
        {this.state.loading &&
          <Message>
            <Image src={IconLoading} alt="" />
            <Text>Please wait a moment...</Text>
          </Message>
        }
        {Object.keys(this.state.details).length > 0 ? (
          !this.state.loading &&
          <Details details={this.state.details} events={this.state.events} />
        ) : (
          !this.state.loading &&
          <Message>
            <Image src={IconEmpty} alt="" />
            <Text>Search for an artist or band above!</Text>
          </Message>
        )}
      </ArtistsStyled>
    );
  }
}

export default Artists;
