import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Artists from '../artists/Artists';

import { AppStyled } from './styled';

const App = () => (
  <AppStyled data-testid="app">
    <Switch>
      <Route exact path="/" component={Artists} />
      <Redirect to="/" />
    </Switch>
  </AppStyled>
);

export default App;
