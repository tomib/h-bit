import styled from 'styled-components';

import { colors } from '../../constants/colors';

export const AppStyled = styled.div`
  width: 100%;

  background: ${colors.base};

  @media (min-width: 760px) {
    width: 95%;
    max-width: 960px;

    margin: 20px auto;

    box-shadow: 0px 2px 30px 0px rgba(0,0,0,0.05);
  }
`;

export default AppStyled;
